local thisversion = 1.0

local function loadscript()
	miniAch = RegisterMod("mini achievements", 1)
	miniAch.Version = thisversion
	miniAch.Loaded = false
	local PATHPREFIX = "scripts/mini_achievements/"
	print("[".. miniAch.Name .."]", "loading script V" .. miniAch.Version .. "...")
	
	local achqueue = {}
	local achsprite = Sprite()
	local justshowed = false
	local sfx = SFXManager()
	
	function miniAch.AddToQueue(gfx, sfx, paper, shadername)
		if gfx == nil then gfx = "gfx/ui/miniachievement/default.png" end
		if sfx == nil then sfx = SoundEffect.SOUND_BOOK_PAGE_TURN_12 end
		if paper == nil then paper = PATHPREFIX .. "asset/miniachievementpaper.png" end
		if shadername == nil then shadername = "MINIACH_NOSHADER" end
		achqueue[#achqueue + 1] = {"gfx/ui/miniachievement/" .. gfx, sfx, paper, shadername}
	end
	
	local function shiftdown(t)
		for i = 1, #t do
			t[i] = t[i + 1]
			t[i + 1] = nil
		end
	end
	
	achsprite:Load(PATHPREFIX .. "asset/mini_achievement.anm2", true)
	
	local currframe = 0
	
	function miniAch:onRender()
		
		local currach = achqueue[1]
		
		if currach ~= nil and currach[4] == "MINIACH_NOSHADER" then
			if currframe < 2 then currframe = currframe + 1 end
			
			if currframe == 2 then
				achsprite:Update()
				currframe = 0
			end
			
			if not justshowed then
				achsprite:ReplaceSpritesheet(0, currach[3])
				achsprite:ReplaceSpritesheet(1, currach[1])
				achsprite:LoadGraphics()
				achsprite:Play("Appear", false)
				sfx:Play(currach[2], 1, 0, false, 1)
				justshowed = true
			end
			if achsprite:IsFinished("Appear") then
				achsprite:Play("Idle", true)
			elseif achsprite:IsFinished("Idle") then
				achsprite:Play("Disappear", true)
			elseif achsprite:IsFinished("Disappear") then
				justshowed = false
				shiftdown(achqueue)
			end
			achsprite:Render(Vector(Isaac.GetScreenWidth()/2, Isaac.GetScreenHeight() - 48), Vector.Zero, Vector.Zero)
		end
	end
	miniAch:AddCallback(ModCallbacks.MC_POST_RENDER, miniAch.onRender)
	
	function miniAch:onRenderTrue(shadername)
		
		local currach = achqueue[1]
		
		if currach ~= nil and shadername == currach[4] then
			if currframe < 2 then currframe = currframe + 1 end
			
			if currframe == 2 then
				achsprite:Update()
				currframe = 0
			end
			
			if not justshowed then
				achsprite:ReplaceSpritesheet(0, currach[3])
				achsprite:ReplaceSpritesheet(1, currach[1])
				achsprite:LoadGraphics()
				achsprite:Play("Appear", false)
				sfx:Play(currach[2], 1, 0, false, 1)
				justshowed = true
			end
			if achsprite:IsFinished("Appear") then
				achsprite:Play("Idle", true)
			elseif achsprite:IsFinished("Idle") then
				achsprite:Play("Disappear", true)
			elseif achsprite:IsFinished("Disappear") then
				justshowed = false
				shiftdown(achqueue)
			end
			achsprite:Render(Vector(Isaac.GetScreenWidth()/2, Isaac.GetScreenHeight() - 48), Vector.Zero, Vector.Zero)
		end
	end
	miniAch:AddCallback(ModCallbacks.MC_GET_SHADER_PARAMS, miniAch.onRenderTrue)
	
	miniAch.Loaded = true
	print("[".. miniAch.Name .."]", "loaded successfully")
end

if miniAch then
	if miniAch.Version < thisversion then
		print("[".. miniAch.Name .."]", "found old script V" .. miniAch.Version .. ", found new script V" .. thisversion .. ". replacing...")
		miniAch = nil
		loadscript()
		print("[".. miniAch.Name .."]", "replaced with V" .. miniAch.Version)
	end
elseif not miniAch then
	loadscript()
end

